import 'package:bowling_calculator_app/view/bowlingView.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'BowlingApp', home: BowlingView());
  }
}
