import 'package:bowling_calculator_app/model/bowlingFrame.dart';

class BowlingGame {
  List<BowlingFrame> rolls = [
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
    BowlingFrame(),
  ];

  int currentFrame = 0;
  int currentRollInFrame = 0;
  int score = 0;

  void roll(int rollResult) {
    //Extra roll?
    if ((currentFrame == 10 &&
            rolls[currentFrame].firstRoll == 10 &&
            currentRollInFrame >= 22) ||
        currentFrame == 10 &&
            rolls[currentFrame].firstRoll + rolls[currentFrame].secondRoll ==
                10) {
      rolls[currentFrame].extraRoll = rollResult;
    } else if (currentRollInFrame.isEven) {
      rolls[currentFrame].firstRoll = rollResult;
      currentRollInFrame++;
      if(rollResult == 10 && currentFrame != 10) {
        currentRollInFrame++;
        currentFrame++;
      }
    } else {
      rolls[currentFrame].secondRoll = rollResult;
      if (currentFrame < 10) {
        currentFrame++;
      }
      currentRollInFrame++;
    }
  }

  int calculateScore() {
    int score = 0;

    for (int frame = 0; frame < rolls.length; frame++) {
      //Scoring spare
      if (rolls[frame].firstRoll + rolls[frame].secondRoll == 10) {
        //Scoring spare in last frame.
        if (currentFrame == 10 && currentRollInFrame >= 22) {
          score += rolls[frame].firstRoll +
              rolls[frame].secondRoll +
              rolls[frame].extraRoll;
        } else {
          score += rolls[frame].firstRoll +
              rolls[frame].secondRoll +
              rolls[frame + 1].firstRoll;
        }
      }
      //Scoring strike
      else if (rolls[frame].firstRoll == 10) {
        if (currentFrame == 10) {
          score += rolls[frame].firstRoll +
              (rolls[frame].secondRoll * 2) +
              (rolls[frame].extraRoll * 2);
        } else {
          if(rolls[currentFrame + 1].firstRoll == 10) {
            score += rolls[frame].firstRoll +
              rolls[frame + 1].firstRoll +
              rolls[frame + 2].firstRoll;
          } else {
            score += rolls[frame].firstRoll +
              rolls[frame + 1].firstRoll +
              rolls[frame + 1].secondRoll;
          }
        }
      } else {
        score += rolls[frame].firstRoll + rolls[frame].secondRoll;
      }
    }
    return score;
  }
}
