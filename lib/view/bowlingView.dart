import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bowling_calculator_app/model/bowlingGame.dart';

class BowlingView extends StatefulWidget {
  BowlingView({Key key}) : super(key: key);

  @override
  _BowlingViewState createState() => _BowlingViewState();
}

class _BowlingViewState extends State<BowlingView> {
  final bowlingGame = new BowlingGame();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: bowlingGame.rolls.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      width: MediaQuery.of(context).size.width / 20,
                      height: MediaQuery.of(context).size.height / 20,
                      color: Colors.amber,
                      child: Row(children: [
                        Text(((bowlingGame.rolls[index].firstRoll).toString()),
                            textAlign: TextAlign.center),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                        ),
                        Text(((bowlingGame.rolls[index].secondRoll).toString()),
                            textAlign: TextAlign.center)
                      ]));
                }),
            flex: 4),
        Expanded(
            flex: 1,
            child: TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(contentPadding: EdgeInsets.all(5)),
                maxLength: 2,
                onSubmitted: (value) {
                  setState(
                    () => bowlingGame.roll(int.parse(value)),
                  );
                })),
        Expanded(
          child: Row(children: [
            Text(bowlingGame.score.toString()),
            TextButton(
                style: TextButton.styleFrom(
                  textStyle: const TextStyle(fontSize: 10),
                ),
                child: const Text('Calculate Score'),
                onPressed: () => setState(
                    () => bowlingGame.score = bowlingGame.calculateScore()))
          ]),
        )
      ],
    ));
  }
}
