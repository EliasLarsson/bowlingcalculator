import 'package:bowling_calculator_app/model/bowlingGame.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  final testGame = new BowlingGame();

    test('randomScore', () {
     
      testGame.roll(1);
      testGame.roll(1);
      testGame.roll(3);
      testGame.roll(4);
      testGame.roll(5);
      testGame.roll(5);
      testGame.roll(6);
      testGame.roll(1);
      testGame.roll(7);
      testGame.roll(1);
      testGame.roll(8);
      testGame.roll(2);
      testGame.roll(9);
      testGame.roll(1);
      testGame.roll(10);
      testGame.roll(1);
      testGame.roll(1);
      testGame.roll(10);
      testGame.roll(1);
      testGame.roll(1);
      
      int score = testGame.calculateScore();

      expect(score, 105);
    });
}